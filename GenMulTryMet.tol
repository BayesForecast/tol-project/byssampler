//////////////////////////////////////////////////////////////////////////////
// FILE    : GenMulTryMet.tol
// PURPOSE : Implementation of Class BysMulTryMet::@GenMulTryMet
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Generalized Multiple Try Metropolis sampler (GMTM)
Class @GenMulTryMet : @AcceptReject
//////////////////////////////////////////////////////////////////////////////
{

  ///////////////////////////////////////////////////////////////////////////
  //Tunning control members
  ///////////////////////////////////////////////////////////////////////////
  //Current number of tries by iteration
  Real _.k = 10;

  //Log-Weight function
  Real logWeight(VMatrix u, VMatrix v);

  ////////////////////////////////////////////////////////////////////////////
  //Initialize
  Real init.GenMulTryMet (Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE @GenMulTryMet::init.GenMulTryMet");
    If(IsUnknown(acceptRatio.target),
      acceptRatio.target := 1- (1 - 0.234  + (0.5-0.234)*Exp(1-_.n))^_.k);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Prepare
  Real logWeight.prepare(VMatrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    0
  };

  ////////////////////////////////////////////////////////////////////////////
  //Draws a simulation by multiple try Metropolis method
  VMatrix _Q_draw_and_prob(VMatrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [_Q_draw_and_prob] 1");
    Real hMax = logWeight.prepare(x);
    If(hMax, 
    {
      Real _.acceptanceProb := If(!IsFinite(_.pi_logDens_y), 0,
        Min(1, Exp(_.pi_logDens_y-_.pi_logDens_x)));
      VMatrix y = x+_.u*hMax
    },
    {
      Set Y = For(1,_.k, VMatrix Y_Q_draw(Real j) { Q_draw(x) });
    //WriteLn("TRACE   Y = "<<EvalSet(Y,Matrix(VMatrix Yj) { VMat2Mat(Yj,True) }));
    //WriteLn("TRACE [_Q_draw_and_prob] 1.1");
      Matrix lwYjx = SetCol(For(1,_.k, Real(Real j) { logWeight(Y[j],x) }));
    //WriteLn("TRACE [_Q_draw_and_prob] 2");
      Real j = MatDat(CppTools::RandPropLogWeight(lwYjx,1),1,1);
    //WriteLn("TRACE [_Q_draw_and_prob] 3");
      VMatrix y = Y[j];
      Real _.pi_logDens_y := pi_logDens(y);
      Real lwy = MatDat(lwYjx,j,1);
      Set X = For(1,_.k-1, VMatrix X_Q_draw(Real j) { Q_draw(y) }) << [[x]];
    //WriteLn("TRACE [_Q_draw_and_prob] 3.1");
      Matrix lwXjy = SetCol(For(1,_.k, Real(Real j) { logWeight(X[j],y) }));
    //WriteLn("TRACE [_Q_draw_and_prob] 3.2");
      Real lwx = MatDat(lwXjy,_.k,1);
      Real log_sum_wYjx = ?; 
      Real log_sum_wXjy = ?; 
    //WriteLn("TRACE [_Q_draw_and_prob] 4");
      Real quot_lw = CppTools::QuotientOfWeightSumsFromLog
        (lwYjx, lwXjy, log_sum_wYjx, log_sum_wXjy);
    //WriteLn("TRACE [_Q_draw_and_prob] 5");
    //WriteLn("TRACE   Q_logDensDif(y,x) = "<<Q_logDensDif(y,x));
    //WriteLn("TRACE   lwx = "<<lwx);
    //WriteLn("TRACE   lwy = "<<lwy);
    //WriteLn("TRACE   lwYjx = "<<Matrix Tra(lwYjx)); 
    //WriteLn("TRACE   lwXjy = "<<Matrix Tra(lwXjy));
    //WriteLn("TRACE   log_sum_wYjx = "<<log_sum_wYjx);
    //WriteLn("TRACE   log_sum_wXjy = "<<log_sum_wXjy);
      Real _.acceptanceProb := If(!IsFinite(_.pi_logDens_y), 0,
        Min(1,Exp(_.pi_logDens_y-_.pi_logDens_x+Q_logDensDif(y,x) + 
            lwx - lwy + log_sum_wYjx - log_sum_wXjy)));
    //WriteLn("TRACE _.acceptanceProb = "<<_.acceptanceProb);
      y
    })
  }

};

//////////////////////////////////////////////////////////////////////////////
//Random Ray Generalized Multiple Try Metropolis sampler (RRGMTM)
Class @GenMulTryMet.RandRay : @GenMulTryMet, @RandRay
//////////////////////////////////////////////////////////////////////////////
{
  ////////////////////////////////////////////////////////////////////////////
  Real init.GenMulTryMet.RandRay(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE @GenMulTryMet.RandRay::init.GenMulTryMet.RandRay");
    Real init.RandRay(void);
    Real init.GenMulTryMet(void);
    True
  };
  ///////////////////////////////////////////////////////////////////////////
  Real initialize(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE @GenMulTryMet.RandRay.Exact::initialize");
    init.GenMulTryMet.RandRay(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
//Random Ray Generalized Multiple Try Metropolis sampler (RRGMTM)
Class @GenMulTryMet.RandRay.Exact : @GenMulTryMet.RandRay
//This sampler is the same than @MulTryMet.RandRay and is used just for 
//checking and diagnosis
//////////////////////////////////////////////////////////////////////////////
{
  ///////////////////////////////////////////////////////////////////////////
  Real logWeight(VMatrix u, VMatrix v)
  ///////////////////////////////////////////////////////////////////////////
  {
    pi_logDens(u)
  }
};


//////////////////////////////////////////////////////////////////////////////
//Random Ray Generalized Multiple Try Metropolis sampler (RRGMTM)
Class @GenMulTryMet.RandRay.Interp : @GenMulTryMet.RandRay
//////////////////////////////////////////////////////////////////////////////
{
  Real _.interp.sideNum = 1;
  Text interp.method = "CubicSpline";
  Real interp.order = 0;
  Set _interp.handler = Copy(Empty); 
  Real _.uRefCoord = ?;
  Set _.interpolation = Copy(Empty); 
  Real _.interpSize = ?;
  Real log.range = Log(10);
  Real _.lpi_range_old = +1/0;

  /////////////////////////////////////////////////////////////////////////////
  Set _eval_h(Real j)
  /////////////////////////////////////////////////////////////////////////////
  {
    Real h = _.interpSize * j/sn;
    Real lpi = If(h==0, _.pi_logDens_x, pi_logDens(x+_.u*h));
  //WriteLn("TRACE [_eval_h j="<<j+" h="<<h+" lpi="<<lpi);
    [[h, lpi]]
  };

  /////////////////////////////////////////////////////////////////////////////
  Real _findMaxRow(Matrix u)
  /////////////////////////////////////////////////////////////////////////////
  {
    MatMax(DifEq(1/(1-B),Constant(Rows(u),1,1)) $* 
    EQ(u,u*0+MatMax(u)))
  };
  /////////////////////////////////////////////////////////////////////////////
  Real _findMaxRowV(VMatrix u)
  /////////////////////////////////////////////////////////////////////////////
  {
    VMatMax(DifEq(1/(1-B),Constant(VRows(u),1,1)) $* 
    EQ(u,u*0+VMatMax(u)))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Tunnes step size (Arguments are not used)
  Real tunning(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_.lpi_range_old < log.range, 
      Real _.stepSize := _.stepSize * (1+2*_.interp.sideNum) );
    True
  };

  /////////////////////////////////////////////////////////////////////////////
  Real logWeight.prepare(VMatrix x)
  /////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [logWeight.prepare] 1 _.lpi_range_old="<<_.lpi_range_old);
  //WriteLn("TRACE [logWeight.prepare] 1.1 _.pi_logDens_x="<<_.pi_logDens_x);
    Real _.interpSize := 2 * _.stepSize;
    Real sz = _.interpSize;
    Real sn = _.interp.sideNum;

    Real end = False;
    Real numTry = 0;
    Real lpi_a = -1/0;
    Real lpi_b = -1/0;
    While(!end,
    {
      Real lpi_a := pi_logDens(x-_.u*sz);
      Real lpi_b := pi_logDens(x+_.u*sz);
      Real end := Or( And(lpi_a > _.pi_logDens_x - log.range, lpi_b>-1/0),
                      And(lpi_b > _.pi_logDens_x - log.range, lpi_a>-1/0) );
    //WriteLn("TRACE [logWeight.prepare] 2."<<numTry+" sz = "<<sz+" lpi_a = "<<lpi_a+" lpi_b = "<<lpi_b);
      If(!end, sz := sz / (1+2*sn));
      Real numTry := numTry + 1
    });
    Real _.interpSize := sz;
    Real _.stepSize := sz/2;
  //WriteLn("TRACE [logWeight.prepare] 3 sz = "<<sz+" lpi_a = "<<lpi_a+" lpi_b = "<<lpi_b);
    Matrix HL = SetMat(
      {[[ [[Real h=-sz, Real lpi_a]] ]]} <<
      For(-sn+1, sn-1,_eval_h) <<
      {[[ [[Real h=+sz, Real lpi_b]] ]] }); 
  //WriteLn("TRACE [logWeight.prepare] 4 HL=\n"<<HL);
    Matrix H = SubCol(HL,[[1]]);
    Matrix L = SubCol(HL,[[2]]);
     
    Real _.lpi_range_old := MatMax(L) - _.pi_logDens_x;
    Real maxLpiCoord = _findMaxRow(L);
  //WriteLn("TRACE [logWeight.prepare] 4.1 _.lpi_range_old="<<_.lpi_range_old);
  //WriteLn("TRACE [logWeight.prepare] 4.2 maxLpiCoord="<<maxLpiCoord);
    Real hMax = If(_.lpi_range_old>=Log(100), 
    {
      Real _.pi_logDens_y := MatDat(HL,maxLpiCoord,2);
      MatDat(HL,maxLpiCoord,1)
    },
    {
      Set _.interpolation := [[
        Code eval = AlgLib.Interp.Scalar(interp.method,H, L, interp.order) ]];
    //WriteLn("TRACE [logWeight.prepare] 5");
      Real _.uRefCoord := _findMaxRowV(_.u);
    //WriteLn("TRACE [logWeight.prepare] 6 _.uRefCoord = "<<_.uRefCoord);
      0
    });
  //WriteLn("TRACE [logWeight.prepare] 4.3 hMax="<<hMax);
    hMax
  };

  /////////////////////////////////////////////////////////////////////////////
  Real logWeight(VMatrix u, VMatrix v)
  /////////////////////////////////////////////////////////////////////////////
  {
  //pi_logDens(u)
  //WriteLn("TRACE [logWeight] 1");
    Real h = (VMatDat(  v,_.uRefCoord,1)-VMatDat(u,_.uRefCoord,1))
            / VMatDat(_.u,_.uRefCoord,1);
  //WriteLn("TRACE [logWeight] 2");
    Code ev = _.interpolation::eval;
  //WriteLn("TRACE [logWeight] 3");
    Real lw = ev(h);
  //WriteLn("TRACE [logWeight] 4 h="<<h+" lw="<<lw+ " log(pi)="<<pi_logDens(u));
    lw
  }

};

