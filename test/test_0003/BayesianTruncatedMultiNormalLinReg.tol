//#Embed "ConstrMultiNormalLinReg.tol";

//////////////////////////////////////////////////////////////////////////////
Class @BayesianTruncatedMultiNormalLinReg 
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix b0 = Constant(0,0,?);
  VMatrix X = Constant(0,0,?);
  VMatrix Y = Constant(0,0,?);
  VMatrix A = Constant(0,0,?);
  VMatrix a = Constant(0,0,?);
  VMatrix G = Constant(0,0,?);
  VMatrix g = Constant(0,0,?);
  VMatrix lambda.scale = Constant(0,0,?);
  VMatrix lambda.weight = Constant(0,0,?);
  VMatrix LAMBDA.si = Constant(0,0,?);

  Matrix MCMC.sigma  = Constant(0,0,?);
  Matrix MCMC.lambda = Constant(0,0,?);
  Matrix MCMC.beta   = Constant(0,0,?);
  Matrix MCMC.lmh    = Constant(0,0,?);
  Matrix MCMC.llh    = Constant(0,0,?);

  Real tau = ?;
  Real n = ?;
  Real m = ?;
  Real m_ = ?;
  Real r = ?;

  ////////////////////////////////////////////////////////////////////////////
  Static @BayesianTruncatedMultiNormalLinReg  Create(
    VMatrix X_, VMatrix Y_, 
    VMatrix A_, VMatrix a_, 
    VMatrix G_, VMatrix g_, 
    VMatrix S_, VMatrix W_,
    VMatrix b0_)
  ////////////////////////////////////////////////////////////////////////////
  {
    @BayesianTruncatedMultiNormalLinReg instance =
    [[
      VMatrix X = X_;
      VMatrix Y = Y_;
      VMatrix A = A_;
      VMatrix a = a_;
      VMatrix G = G_;
      VMatrix g = g_;
      VMatrix lambda.scale = S_;
      VMatrix lambda.weight = W_;
      VMatrix b0 = b0_
    ]];
    Real instance::setup(?);
    instance
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setup(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real tau := 1.1;
    Real n := VColumns(X);
    Real m := VRows(X);
    Real m_ := m+VRows(G);
    Real r := VRows(A);
    VMatrix LAMBDA.si := Eye(n,n,0,lambda.scale^-1);  
    Matrix MCMC.sigma  := Constant(0,1,?);
    Matrix MCMC.lambda := Constant(0,n,?);
    Matrix MCMC.beta   := Constant(0,n,?);
    Matrix MCMC.llh    := Constant(0,1,?);
    Matrix MCMC.lmh    := Constant(0,1,?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix eval.residuals(VMatrix b_)
  ////////////////////////////////////////////////////////////////////////////
  {
    Group("ConcatColumns",NCopy(VColumns(b_),Y))-X*b_
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix eval.squaresSum(VMatrix b_)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix eps = eval.residuals(b_);
    Constant(1,m,1)*(eps^2)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix draw(Real NumSim)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix res = Y-X*b0;
    Real se = Sqrt(VMatAvr(res^2));
    WriteLn("TRACE [@BayesianTruncatedMultiNormalLinReg::draw] "<<
      NumSim+" se:"<<se);

    VMatrix U = ((X/se) << (LAMBDA.si * G))/tau;
    VMatrix V = ((Y/se) << (LAMBDA.si * g))/tau;
    
    @NameBlock _T = [[_this]];
    Set drawer = { @NameBlock(@ConstrMultiNormalLinReg::Create(
      VMatrix X = U;
      VMatrix Y = V;
      VMatrix A = $_T::A;
      VMatrix a = $_T::a;
      VMatrix b0 = $_T::b0
    )) };
    
    Real NS = NumSim;
    VMatrix b.try = $drawer::draw(NS);
    
    Matrix b.try_ = VMat2Mat(b.try,True);
    
    Matrix LQ = VMat2Mat(-$drawer::Z.ss*.5,True);
    Matrix Q = Exp(LQ-MatMax(LQ));
    
    VMatrix E.iChiSqr = Mat2VMat(EvalMatRows(Constant(NS,1,m), RandChisq),True);
    VMatrix E = Group("ConcatColumns",NCopy(NS,Y))-X*b.try;
    VMatrix RSS = Constant(1,m,1)*(E^2);
    Matrix sigma = VMat2Mat(RSS $/ E.iChiSqr,True)^(.5);
    VMatrix sigma.i = Mat2VMat(sigma^(-1));
    
    VMatrix SIGMA.i = Eye(NS,NS,0,sigma.i);
    
    VMatrix lambda = Mat2VMat(Group("ConcatColumns",
    For(1,n,Matrix(Real k)
    {
      Real s = VMatDat(lambda.scale,k,1);
      Real w = Min(.99,VMatDat(lambda.weight,k,1));
      Matrix lambda.freeDeg = Constant(NS,1,m) * w/(1-w);
      Sqrt((EvalMatRows(lambda.freeDeg, RandChisq)^-1)*(m*s^2))
    })),True);
    //VMatrix lambda = Group("ConcatColumns",NCopy(NS,lambda.scale));
    
    VMatrix Z = Group("ConcatColumns",NCopy(NS,g))-G*b.try;
    
    VMatrix W = (E*SIGMA.i) << (Z $/ lambda);
    VMatrix W.RSS = Tra(Constant(1,m_,1)*(W^2));
    
    Matrix LLH=VMat2Mat(Log(sigma.i)*m-Tra(Constant(1,n,1)*Log(lambda))-W.RSS/2);
    Matrix LH = Exp(LLH-MatMax(LLH));
    
    Matrix lmh = LLH - LQ;
    
    Matrix mh = LH $/ Q;
    
    NameBlock auto.burn.01 = BysSampler::AutoBurnMaxLogPostDens( lmh, .005);
    NameBlock auto.burn.02 = BysSampler::AutoBurnMaxLogPostDens(-lmh, .005);
    
    Set known.idx = MatQuery::SelectRowsFullKnown(Mat2VMat(mh));
    Set valid.idx = Select(known.idx,Real(Real k)
    {
      GT(k,Max(auto.burn.01::burnin,auto.burn.02::burnin))
    });
    
    Set LMH = MatSet(Tra(lmh))[1];
    Set LMH.vi = ExtractByIndex(LMH,valid.idx);
    
    Real k = Card(valid.idx);
    Real lmh.k = LMH[ valid.idx[k] ];
    
    Set MCMC.idx.pos = MatSet(Tra(BysSampler::CppTools::RandPropLogWeight(
      Matrix SubRow(lmh,valid.idx),NS)))[1]; 
    Set MCMC.idx = ExtractByIndex(valid.idx,MCMC.idx.pos);
    
    Matrix mcmc.beta = SubRow(b.try_,MCMC.idx);
    Matrix mcmc.sigma  = SubRow(sigma,MCMC.idx);
    Matrix mcmc.lambda = SubRow(VMat2Mat(lambda,true),MCMC.idx);
    Matrix mcmc.llh = SubRow(LLH,MCMC.idx);
    Matrix mcmc.lmh = SubRow(lmh,MCMC.idx);

    Real AppendRows(MCMC.beta,   mcmc.beta);
    Real AppendRows(MCMC.sigma,  mcmc.sigma);
    Real AppendRows(MCMC.lambda, mcmc.lambda);
    Real AppendRows(MCMC.llh,    mcmc.llh);
    Real AppendRows(MCMC.lmh,    mcmc.lmh);
  
    VMatrix b0 := Mat2VMat(Constant(1,NS,1/NS)*mcmc.beta,True);
    Mat2VMat(mcmc.beta, True)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix draw.batches(Real NumSim, Real batches)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(batches<=1, draw(NumSim),
    Group("ConcatColumns",For(1,batches,VMatrix(Real batch)
    { 
      draw(NumSim/batches)
    })));
    Set MCMC.idx = MatSet(Tra(BysSampler::CppTools::RandPropLogWeight(
      MCMC.lmh, NumSim)))[1]; 
    Matrix MCMC.beta   := SubRow(MCMC.beta,   MCMC.idx);
    Matrix MCMC.sigma  := SubRow(MCMC.sigma,  MCMC.idx);
    Matrix MCMC.lambda := SubRow(MCMC.lambda, MCMC.idx);
    Matrix MCMC.llh    := SubRow(MCMC.llh,    MCMC.idx);
    Matrix MCMC.lmh    := SubRow(MCMC.lmh,    MCMC.idx);
    Mat2VMat(MCMC.beta,True)
  }
};


Real t0 := Copy(Time);

NameBlock drawer = { @BayesianTruncatedMultiNormalLinReg::Create(
  VMatrix X = modDef::X;
  VMatrix Y = modDef::Y;
  VMatrix A = modDef::A;
  VMatrix a = modDef::a;
  VMatrix G = modDef::prior.X;
  VMatrix g = modDef::prior.Y;
  VMatrix lambda.scale = modDef::prior.lambda0;
  VMatrix lambda.weight = Constant(VColumns(X),1,.95);
  VMatrix b0 = modDef::b0
)};

Matrix mcmc = VMat2Mat(drawer::draw.batches(sampleLength,10),True),

Real time.mcmc=Copy(Time)-t0;



Set qChk.02 = modDef::QualityCheck(
  "BayesianTruncatedMultiNormalLinReg",mcmc, burnin, time.mcmc);



/* */

/*
Set MCMC.idx = [[ valid.idx[k] ]] << For(1,sampleLength-1,Real(Real iter)
{
  Real j = IntRand(1,Card(valid.idx));
  Real lmh.j = LMH[ valid.idx[j] ];
  WriteLn("Trying LMH["<<j+"]="<<lmh.j+" versus LMH["<<k+"]="<<lmh.k);
  Real If(lmh.j>=lmh.k, { k:=j; lmh.k:=lmh.j },
  { 
    Real rnd = Rand(0,1);
    Real a = Min(1,Exp(lmh.j-lmh.k));
    WriteLn("  rnd:"<<rnd+" <=> "<<a);
    If(rnd<=a, { k:=j; lmh.k:=lmh.j })
  });
  valid.idx[k]
});
*/

