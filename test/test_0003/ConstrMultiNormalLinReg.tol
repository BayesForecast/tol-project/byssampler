//////////////////////////////////////////////////////////////////////////////
Class @ConstrMultiNormalLinReg 
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix X;
  VMatrix Y;
  VMatrix A;
  VMatrix a;
  VMatrix b0;

  Real n = ?;
  Real m = ?;
  Real r = ?;
  Real sigma = ?;
  Real log.det.LiT = ?;
  VMatrix LiT = Constant(0,0,?);
  VMatrix LiTs = Constant(0,0,?);
  VMatrix nu  = Constant(0,0,?);
  VMatrix d = Constant(0,0,?);
  VMatrix D = Constant(0,0,?);
  VMatrix z0 = Constant(0,0,?);
  VMatrix b = Constant(0,0,?);
  VMatrix z = Constant(0,0,?);
  VMatrix Z = Constant(0,0,?);
  VMatrix B = Constant(0,0,?);
  VMatrix density = Constant(0,0,?);

  ////////////////////////////////////////////////////////////////////////////
  Static @ConstrMultiNormalLinReg  Create(
    VMatrix X_, VMatrix Y_, VMatrix A_, VMatrix a_, VMatrix b0_)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ConstrMultiNormalLinReg instance =
    [[
      VMatrix X = X_;
      VMatrix Y = Y_;
      VMatrix A = A_;
      VMatrix a = a_;
      VMatrix b0 = b0_
    ]];
    Real instance::setup(?);
    instance
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setup(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix XtY = Tra(Tra(Y)*X);
    VMatrix LiT := CholeskiFactor(X,"XtX");
    VMatrix LiTs := CholeskiSolve(LiT,Convert(LiT, "Cholmod.R.Sparse"),"P");
    Real log.det.LiT := VMatSum(SubDiag(LiTs));
    VMatrix nu  := CholeskiSolve(LiT,XtY,"PtLLtP");
    VMatrix res = Y-X*nu;
    Real sigma := Sqrt(VMatAvr(res^2));
    Real n := VColumns(LiT);
    Real m := VRows(X);
    Real r := VRows(A);
    If(VRows(A), 
    {
      VMatrix d  := a - A*nu;
      VMatrix D  := Tra(CholeskiSolve(LiT,Tra(A),"PtL"))*sigma ;
      VMatrix z0_ = Tra(Tra(b0-nu)*LiT) / sigma;
      VMatrix z0 := BysPrior::@MultNormal.Trunc::
       TryOrFindPointInPolytope(D,d,z0_,BysSampler::_.borderTolerance);
      VMatrix b0 := nu + CholeskiSolve(LiT,z0*sigma,"LtP");
      True
    });
    VMatrix z := z0;
    VMatrix b := b0;
   True
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix eval.residuals(VMatrix b_)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real NS = VColumns(b_);
    VMatrix NY = Mat2VMat(Group("ConcatColumns",NCopy(NS,VMat2Mat(Y))));
    NY-X*b_
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix eval.squaresSum(VMatrix b_)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix eps = eval.residuals(b_);
    Constant(1,m,1)*(eps^2)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix calc.density.V(VMatrix beta)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real NS = VColumns(beta);
    VMatrix NU = Mat2VMat(Group("ConcatColumns",NCopy(NS,VMat2Mat(nu))));
//  VMatrix z_ = Tra(Tra(beta-NU)*LiTs)/sigma;
//  -Constant(1,n,1)*(z_^2)/2-(n*Log(sigma))+log.det.LiT
    VMatrix z_ = Tra(Tra(beta-NU)*LiTs);
    -(z_^2)/2
  };

  ////////////////////////////////////////////////////////////////////////////
  Real calc.density.R(VMatrix beta)
  ////////////////////////////////////////////////////////////////////////////
  {
//  VMatrix z_ = Tra(Tra(beta-nu)*LiTs)/sigma;
//  -VMatSum(z_^2)/2-(n*Log(sigma))+log.det.LiT
    VMatrix z_ = Tra(Tra(beta-nu)*LiTs);
    -VMatSum(z_^2)/2
  };

  ////////////////////////////////////////////////////////////////////////////
  Real change.nu(VMatrix nu_)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(VMatMax(Abs(nu-nu_))==0,False,
    {
      VMatrix nu  := nu_;
      If(VRows(A), 
      {
        VMatrix d  := a - A*nu;
        VMatrix z0_ = Tra(Tra(b0-nu)*LiT) / sigma;
        VMatrix z0 := BysPrior::@MultNormal.Trunc::
         TryOrFindPointInPolytope(D,d,z0_,BysSampler::_.borderTolerance);
        VMatrix b0 := nu + CholeskiSolve(LiT,z0*sigma,"LtP");
        True
      });
      VMatrix z := z0;
      VMatrix b := b0;
      True
    })
  };


  ////////////////////////////////////////////////////////////////////////////
  Real change.Y(VMatrix Y_)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(VMatMax(Abs(Y-Y_))==0,False,
    {
      VMatrix Y := Y_;
      VMatrix XtY = Tra(Tra(Y)*X);
      change.nu(CholeskiSolve(LiT,XtY,"PtLLtP"))
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real change.XY(VMatrix X_,VMatrix Y_)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(VMatMax(Abs(X-X_))==0,
    {
      If(VMatMax(Abs(Y-Y_))==0,False,
      {
        VMatrix Y := Y_;
        VMatrix XtY = Tra(Tra(Y)*X);
        change.nu(CholeskiSolve(LiT,XtY,"PtLLtP"))
      })
    },
    {
      VMatrix X := X_;
      VMatrix Y := Y_;
      setup(?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix draw(Real NumSim)
  ////////////////////////////////////////////////////////////////////////////
  {
    //WriteLn("TRACE [@ConstrMultiNormalLinReg::draw] "<<NumSim);
    VMatrix Z := If(!VRows(A), 
    {
      Gaussian(n,NumSim,0,1)
    },
    {
      TruncStdGaussian(D,d,z,NumSim,0,BysSampler::_.borderTolerance)    
    });
    VMatrix density := 
     //-Constant(1,n,1)*(Z^2)/2-(n*Log(sigma))+log.det.LiT;
       -Constant(1,n,1)*(Z^2)/2;
    VMatrix z := SubCol(Z,[[NumSim]]);
    VMatrix NU = Mat2VMat(Group("ConcatColumns",NCopy(NumSim,VMat2Mat(nu))));
    VMatrix B := NU + CholeskiSolve(LiT,Z*sigma,"LtP");
    VMatrix b := SubCol(B,[[NumSim]]);
    B
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix draw.batches(Real NumSim, Real batches)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(batches<=1, draw(NumSim),
    Group("ConcatColumns",For(1,batches,VMatrix(Real batch)
    { 
      draw(NumSim/batches)
    })))
  }
};

